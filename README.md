# timeseries

A project I did on predicting the spread between crude oil and gasoline. It utilized SciKit-Learn and XGBoost to predict changes in the spread.