"""

"""

import numpy as np
import pandas as pd
import pandas_market_calendars as mcal

from xgboost import XGBRegressor


def process_data(
        df: pd.DataFrame,
        select_cols: [str],
        diff_width: int,
        lags: [int],
        start_date: str,
        end_date: str,
        percentage: bool = True
):
    """

    :param df:
    :param select_cols:
    :param diff_width:
    :param lags:
    :param start_date:
    :param end_date:
    :param percentage:
    :return:
        Start Date, End Date, DataFrame
    """
    df = df.fillna(method="ffill")
    df = df.reindex(mcal.get_calendar('NYSE').valid_days(start_date=start_date, end_date=end_date).date)
    df = df[select_cols]

    output = []
    for column in df.columns:
        output += calc_lags_and_diffs(df[column], diff_width, lags, percentage)

    output = pd.concat(output, axis=1)
    output = output.dropna()

    return min(output.index), max(output.index), output


def calc_lags_and_diffs(series2diff: pd.Series, diff_width: int, lags: [int], percentage: bool = True):
    """

    :param series2diff:
    :param diff_width:
    :param lags:
    :param percentage:
    :return:
    """

    if percentage:
        new_series = (series2diff / series2diff.shift(diff_width) - 1).dropna()

    else:
        new_series = (series2diff - series2diff.shift(10)).dropna()

    output = []
    for lag in lags:
        series2append = new_series.shift(lag).dropna()
        series2append.name = "{}_d{}_l{}".format(series2diff.name, diff_width, lag)
        output.append(series2append)

    return output


def get_start_dates(df: pd.DataFrame):
    output = {}
    max_date = None
    for column in df.columns:
        sd = df[column].dropna().index[0]
        output[column] = sd

        if max_date is None or sd >= max_date:
            max_date = sd

    return max_date, output


def transform_labels(y: pd.Series, pc: float, higher: bool = True):
    """

    :param higher:
    :param y:
    :param pc:
    :return:
        Cutoff Value, Transformed Labels
    """
    cutoff = np.percentile(y, pc)

    if higher:
        return cutoff, y.apply(lambda x: int(x >= cutoff))

    return cutoff, y.apply(lambda x: int(x <= cutoff))


def make_param_grid(params: {}):
    """

    :param params:
    :return:
    """

    result = pd.DataFrame()

    for key, value in params.items():
        block = []
        for v in value:
            temp_df = pd.concat([result, pd.DataFrame([v], columns=["super_param_{}".format(key)])], axis=1)

            block.append(temp_df)

        result = pd.concat(block)

    result = result.reset_index(drop=True)

    return result

