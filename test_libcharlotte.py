import pandas as pd
import pytest

from libcharlotte import transform_labels, make_param_grid


def test_transform_labels_40_high():
    y_init = pd.Series([-0.5, -0.3, -0.1, 0.0, 0.3, 0.7, 1.0, 12])
    y_transform_true = pd.Series([0, 0, 0, 1, 1, 1, 1, 1])
    cutoff, y_t = transform_labels(y_init, 40, higher=True)

    assert y_t.equals(y_transform_true)


def test_transform_labels_55_high():
    y_init = pd.Series([-0.5, -0.3, -0.1, 0.0, 0.3, 0.7, 1.0, 12])
    y_transform_true = pd.Series([0, 0, 0, 0, 1, 1, 1, 1])
    cutoff, y_t = transform_labels(y_init, 55, higher=True)

    assert y_t.equals(y_transform_true)


def test_transform_labels_0_low():
    y_init = pd.Series([-0.3, -0.5, -0.1, 0.0, 0.3, 0.7, 1.0, 12])
    y_transform_true = pd.Series([0, 1, 0, 0, 0, 0, 0, 0])
    cutoff, y_t = transform_labels(y_init, 0, higher=False)

    assert y_t.equals(y_transform_true)


def test_transform_labels_40_low():
    y_init = pd.Series([-0.5, 1.0, -0.3, 0.7, 12, -0.1, 0.0, 0.3])
    y_transform_true = pd.Series([1, 0, 1, 0, 0, 1, 0, 0])
    cutoff, y_t = transform_labels(y_init, 40, higher=False)

    assert y_t.equals(y_transform_true)


def test_make_param_grid_easy():
    p_grid = {'a': [1, 2], 'b': [3, 4], 'c': [50, 70, 99]}

    answer = pd.DataFrame(
        [[1, 3, 50],
         [2, 3, 50],
         [1, 4, 50],
         [2, 4, 50],
         [1, 3, 70],
         [2, 3, 70],
         [1, 4, 70],
         [2, 4, 70],
         [1, 3, 99],
         [2, 3, 99],
         [1, 4, 99],
         [2, 4, 99]],
        columns=['a', 'b', 'c'])
    test_val = make_param_grid(p_grid)

    assert answer.equals(test_val)
